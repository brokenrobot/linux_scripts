#!/bin/sh

# the directory that password files will be stored in.
# this location should be secured from anyone else accessing it since it will contain plaintext passwords.
# each file should contain only the password used for authenticating to the server. (it does not contain the username.)
PASSFILE_DIRECTORY="/home/roland/.ipmi"

# usage syntax
USAGE="Usage: $0 hostname username password_file command...\n\tAll parameters are required.\n\tpassword_file is the path to a file containing the password for the user. (see ipmitool -f for more information)."

# check system for prerequisites
validate_ipmi_supported() {
	if command -v ipmitool > /dev/null 2>&1
	then
		return 0
	else
		echo "This scripts requires the ipmitool command. On Debian try: apt-get install ipmitool"
		return 1
	fi
}

execute_ipmi() {
	ipmitool -I lanplus -H $1 -U $2 -f $3 $4
}

# begin main script
validate_ipmi_supported
VALID_SYSTEM=$?
if [ $VALID_SYSTEM -eq 0 ]
then
	# start checking the supplied parameters
	if [ $# -le 3 ]
	then
		# not enough parameters provided
		echo $USAGE
	else
		# make sure the password file is accessible
		IPMI_PASSFILE="$PASSFILE_DIRECTORY/$3"
		if [ -f "$IPMI_PASSFILE" ]
		then
			# read the rest of the parameters
			IPMI_HOST=$1
			IPMI_USER=$2
			shift 3
			IPMI_COMMAND=$@

			execute_ipmi $IPMI_HOST $IPMI_USER $IPMI_PASSFILE "$IPMI_COMMAND"
		else
			echo "Supplied password file does not exist or cannot be read: $IPMI_PASSFILE"
		fi
	fi
fi
